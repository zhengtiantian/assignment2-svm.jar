 1. Make sure your computer has installed JDK1.8 and has configured environment variables
 2. Download the folder containing svm.jar and a model.txt to your computer. Address is in README.txt
 3. If you want to regenerate the model, put train-io.txt in the same level directory as svm.jar, if you only want to generate results, put only test-in.txt in the same level directory as svm.jar,The file format is given by you
 4. Open the terminal, find the directory where svm.jar is located and run "java -jar svm.jar", test-out.txt will appear in the directory where svm.jar is located

